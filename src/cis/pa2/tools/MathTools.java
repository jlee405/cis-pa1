package cis.pa2.tools;

import Jama.Matrix;

public class MathTools {
	/**
	 * Function to return the minimum of a matrix. Used because JAMA didn't
	 * include a minimum finder.
	 * @param m the matrix input
	 * @return the minimum value of the matrix
	 */
	@SuppressWarnings("null")
	public static double min(Matrix m) {
		double d = m.get(0, 0);
		for (int i = 0; i < m.getRowDimension(); i++) {
			for (int j = 0; j < m.getColumnDimension(); j++) {
				if (d > m.get(i, j)) {
					d = m.get(i,j);
				}
			}
		}
		return d;
	}
	public static double max(Matrix m) {
		double d = m.get(0, 0);
		for (int i = 0; i < m.getRowDimension(); i++) {
			for (int j = 0; j < m.getColumnDimension(); j++) {
				if (d < m.get(i, j)) {
					d = m.get(i,j);
				}
			}
		}
		return d;
	}
}