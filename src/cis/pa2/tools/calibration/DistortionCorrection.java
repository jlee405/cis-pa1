package cis.pa2.tools.calibration;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa2.tools.Bernstein;
import cis.pa2.tools.MathTools;

public class DistortionCorrection {
	private int degrees;
	private Matrix source, target, scaledSource;
	private Matrix C, F;
	private ColumnVector minVector, maxVector;
	
	/**
	 * Constructor for a distortion correction object in 3-dimensions
	 * @param source the source point cloud
	 * @param target the target point cloud
	 */
	public DistortionCorrection(PointCloud source, PointCloud target, int deg) {
		this.degrees = deg;
		this.source = source.asMatrix().copy();
		this.target = target.asMatrix().copy().transpose();
		this.calculateMin();
		this.calculateMax();
		this.scaleToBox(this.minVector, this.maxVector);
		this.F = new Matrix(this.source.getColumnDimension(), (int) Math.pow(this.degrees + 1, 3));
		this.calcCorrectionFunction();
		this.calcBernsteinCoefficients();
	}
	
	/**
	 * Constructor for finding the correction coefficients between two matricies
	 * @param source The source matrix
	 * @param target The target matrix
	 * @param deg The degree of Bernstein polynomial to use
	 */
	public DistortionCorrection(Matrix source, Matrix target, int deg) {
		this.degrees = deg;
		this.source = source.copy();
		this.target = target.copy().transpose();
		this.calculateMin();
		this.calculateMax();
		this.scaleToBox(this.minVector, this.maxVector);
		this.F = new Matrix(this.source.getColumnDimension(), (int) Math.pow(this.degrees + 1, 3));
		this.calcCorrectionFunction();
		this.calcBernsteinCoefficients();
	}
	
	/**
	 * Constructor for applying a known correction function to a source matrix to get the target matrix
	 * @param source The source matrix
	 * @param deg The degree of Bernstein polynomial to use
	 * @param coeff The coefficients of the known correction function
	 * @param min The minimum vector for use in scaleToBox
	 * @param max The maximum vector for use in scaleToBox
	 */
	public DistortionCorrection(Matrix source, int deg, Matrix coeff, ColumnVector min, ColumnVector max) {
		this.degrees = deg;
		this.source = source.copy();
		this.C = coeff.copy();
		this.scaleToBox(min, max);
		this.F = new Matrix(this.source.getColumnDimension(), (int) Math.pow(this.degrees + 1, 3));
		this.calcCorrectionFunction();
		this.calcCorrected();
	}
	
	private void calcCorrectionFunction() {
		int jreps = this.degrees + 1;
		int ireps = (int) Math.pow(this.degrees + 1, 2);
		for (int n = 0; n < this.scaledSource.getColumnDimension(); n++) {
			for (int i = 0; i < this.degrees + 1; i++) {
				for (int j = 0; j < this.degrees + 1; j++) {
					for (int k = 0; k < this.degrees + 1; k++) {
						int index = i*ireps + j*jreps + k;
						double bern = Bernstein.polynomial(this.scaledSource.get(0, n), this.degrees, i);
						bern = bern*Bernstein.polynomial(this.scaledSource.get(1, n), this.degrees, j);
						bern = bern*Bernstein.polynomial(this.scaledSource.get(2, n), this.degrees, k);
						this.F.set(n, index, bern);
					}
				}
			}
		}
	}
	
	private void calcCorrected() {
		this.target = this.F.times(this.C);
	}
	
	/**
	 * Scales the source matrix into a new scaled matrix, 
	 * where the scaling box is determined by the min and max vectors
	 * @param min The minimum vector for the box
	 * @param max The maximum vector for the box
	 */
	
	private void scaleToBox(ColumnVector min, ColumnVector max) {
		this.scaledSource = this.source.copy();
		for (int i = 0; i < this.source.getRowDimension(); i++) {
			for (int j = 0; j < this.source.getColumnDimension(); j++) {
				double scaled = (this.source.get(i, j) - min.get(i))/(max.get(i) - min.get(i));
				this.scaledSource.set(i, j, scaled);
			}
		}
	}
	
	/**
	 * Calculates the minimum vector i.e. a vector with values 
	 * set to the minimum in each coordinate of the source matrix
	 */
	private void calculateMin() {
		double[] min = new double[this.source.getRowDimension()];
		for (int i = 0; i < min.length; i++) {
			min[i] = MathTools.min(source.getMatrix(i, i, 0, source.getColumnDimension() - 1));
		}
		this.minVector = new ColumnVector(min);
	}
	
	/**
	 * Calculates the maximum vector i.e. a vector with values 
	 * set to the maximum in each coordinate of the source matrix
	 */	
	private void calculateMax() {
		double[] max = new double[this.source.getRowDimension()];
		for (int i = 0; i < max.length; i++) {
			max[i] = MathTools.max(source.getMatrix(i, i, 0, source.getColumnDimension() - 1));
		}
		this.maxVector = new ColumnVector(max);
	}
	
	
	private void calcBernsteinCoefficients() {
		C = this.F.solve(target);
	}
	public Matrix getBernsteinCoefficients() {
		return this.C;
	}
	public Matrix getCorrectionFunction() {
		return this.F;
	}
	public Matrix getCorrectedMatrix() {
		return this.target;
	}
	public Matrix getCorrectedMatrixTranspose() {
		return this.target.transpose();
	}
	
	public ColumnVector getMinVector() {
		return this.minVector;
	}
	
	public ColumnVector getMaxVector() {
		return this.maxVector;
	}
}
