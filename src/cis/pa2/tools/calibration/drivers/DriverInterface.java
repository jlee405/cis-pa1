package cis.pa2.tools.calibration.drivers;

public interface DriverInterface {
	/**
	 * Runs calibration for this driver on a debug dataset specified by a letter
	 * @param dataHeader The string path of the directory in which the dataset is located
	 * @param printIntermediateSteps Whether to print intermediate steps
	 * @param letter The dataset letter
	 */
	public void calibrationOnDebugDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter);
	
	/**
	 * Runs calibration for this driver on an unknown dataset specified by a letter
	 * @param dataHeader The string path of the directory in which the dataset is located
	 * @param printIntermediateSteps Whether to print intermediate steps
	 * @param letter The dataset letter
	 */
	public void calibrationOnUnknownDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter);


}
