package cis.pa2.tools.calibration.drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.Frame;
import cis.pa1.tools.HornRegistration;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.MidPointPivotCalibration;
import cis.pa1.tools.data.DataFileParser;
import cis.pa2.tools.calibration.DistortionCorrection;
import cis.pa2.tools.data.CTFiducialsData;
import cis.pa2.tools.data.EMFiducialsData;
import cis.pa2.tools.data.EMNavigationData;
import cis.pa2.tools.data.Output2Data;

public final class EMFiducialDriver implements DriverInterface {
	private Frame emToCtFrame;
	private List<ColumnVector> emFiducials;
	
	public Frame Freg() {
		return this.emToCtFrame;
	}
	public List<ColumnVector> ctFiducialsInEm() {
		return this.emFiducials;
	}
	
	/**
	 * Computes the location of the CT fiducials in EM coordinates
	 * @param correctedPostVector The post vector after distortion correction 
	 * @param GFrames The EM markers at the CT fiducials
	 * @param correction The distortion correction function
	 * @return
	 */
	public List<ColumnVector> computeEMFiducials(ColumnVector correctedPostVector, List<PointCloud> GFrames, DistortionCorrection correction) {
		List<PointCloud> correctedFrames = PointCloud.correctPointCloudList(GFrames, correction);
		this.emFiducials = new ArrayList<>();
		
		for (int i = 0 ; i < correctedFrames.size(); i++) {
			/*The midpoint pivot calibration is used to get the frame transformation
			from EM tracker base to the marker coordinate system (origin at the centroid)*/
			MidPointPivotCalibration pcNew = new MidPointPivotCalibration(correctedFrames.get(i), null);			
			Frame currentFiducialFrame = new Frame(pcNew.rotationMatrix(), pcNew.translationVector());
			ColumnVector fiducialInEm = currentFiducialFrame.inverse().timesVector(correctedPostVector);
			this.emFiducials.add(fiducialInEm);
		}
		return this.emFiducials;
	}
	
	/**
	 * Computes the F_reg transformation from EM -> CT 
	 * @param emFiducials The CT fiducials in EM coordinates
	 * @param ctFiducials The CT fiducials in CT coordinates
	 * @return
	 */
	public Frame computeFRegistration(PointCloud emFiducials, PointCloud ctFiducials) {
		HornRegistration registration = new HornRegistration(emFiducials.asMatrix(), ctFiducials.asMatrix(), false);
		this.emToCtFrame =  new Frame(registration.rotationMatrix(), registration.transVector());
		return this.emToCtFrame;
	}
	
	/**
	 * Transforms the given EM navigation points into CT coordinates
	 * @param correctedPostVector The post vector after distortion correction
	 * @param GFrames The distorted EM navigation points
	 * @param correction The distortion correction function
	 * @param Freg The EM-to-CT frame
	 * @return
	 */
	public List<ColumnVector> transformEmNavigationToCt(ColumnVector correctedPostVector, List<PointCloud> GFrames, DistortionCorrection correction, Frame Freg) {
		List<ColumnVector> emNavs = this.computeEMFiducials(correctedPostVector, GFrames, correction);
		List<ColumnVector> ctNavs = new ArrayList<>();
		for (ColumnVector emNav : emNavs) {
			ctNavs.add(Freg.timesVector(emNav));
		}
		return ctNavs;
	}
	
	@Override
    public void calibrationOnDebugDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		EMDistortionCalibrationDriver emDistortionDriver = new EMDistortionCalibrationDriver();
		DistortionCorrectionDriver distortionCorrectionDriver = new DistortionCorrectionDriver();
		
		DataFileParser parser = new DataFileParser();
		
		String header = dataPath + "pa2-debug-";
		String ctFiducialsPath = header + letter + "-ct-fiducials.txt";
		String emFiducialsPath = header + letter + "-em-fiducialss.txt";
		String emNavPath = header + letter + "-EM-nav.txt";
		String output2Path = header + letter + "-output2.txt";
		
		EMFiducialsData emFiducialData = parser.parseEMFiducialsData(emFiducialsPath);
		CTFiducialsData ctFiducialData = parser.parseCTFiducialsData(ctFiducialsPath);
		EMNavigationData emNavData = parser.parseEMNavData(emNavPath);
		Output2Data output2Data = parser.parseOutput2Data(output2Path);
		
		List<PointCloud> GFrames = emFiducialData.getGFrames();
		
		distortionCorrectionDriver.calibrationOnDebugDataset(dataPath, false, false, letter);
		DistortionCorrection correction = distortionCorrectionDriver.getDistortionCorrection();
		emDistortionDriver.calibrationOnDebugDataset(dataPath, false, false, letter);
		ColumnVector correctedPostVector = emDistortionDriver.postVector();
		
		List<ColumnVector> emFiducials = this.computeEMFiducials(correctedPostVector, GFrames, correction);
		PointCloud emFiducialCloud = new PointCloud(emFiducials);
		if (printIntermediateSteps) {
			System.out.println("CT fiducials in EM coordinates after distortion correction for dataset " + letter);
			emFiducialCloud.asMatrix().print(0, 2);
		}
		
		Frame Freg = this.computeFRegistration(emFiducialCloud, ctFiducialData.getB());
		if (printIntermediateSteps) {
			System.out.println("F_reg for dataset " + letter);
			Freg.getRotationMatrix().print(0, 2);
			Freg.getTranslationVector().getMatrix().print(0, 2);
		}
		
		GFrames = emNavData.getGFrames();
		List<ColumnVector> ctNavs = this.transformEmNavigationToCt(correctedPostVector, GFrames, correction, Freg);
		PointCloud ctNavCloud = new PointCloud(ctNavs);
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-debug-" + letter + "-output2.txt"), true));
				writer.println("EM navigation points in CT coordinates for dataset " + letter);
				Matrix ctNavMatrix = ctNavCloud.asMatrix();
				ctNavMatrix.print(writer, 0, 2);
				Matrix expectedOutputMatrix = output2Data.getV().asMatrix();
				writer.println("Difference from expected output.");
				ctNavMatrix.minus(expectedOutputMatrix).print(writer, 0, 2);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}		
    }
	
	@Override
    public void calibrationOnUnknownDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		EMDistortionCalibrationDriver emDistortionDriver = new EMDistortionCalibrationDriver();
		DistortionCorrectionDriver distortionCorrectionDriver = new DistortionCorrectionDriver();
		
		DataFileParser parser = new DataFileParser();
		
		String header = dataPath + "pa2-unknown-";
		String ctFiducialsPath = header + letter + "-ct-fiducials.txt";
		String emFiducialsPath = header + letter + "-em-fiducialss.txt";
		String emNavPath = header + letter + "-EM-nav.txt";
		
		EMFiducialsData emFiducialData = parser.parseEMFiducialsData(emFiducialsPath);
		CTFiducialsData ctFiducialData = parser.parseCTFiducialsData(ctFiducialsPath);
		EMNavigationData emNavData = parser.parseEMNavData(emNavPath);
		
		List<PointCloud> GFrames = emFiducialData.getGFrames();
		
		distortionCorrectionDriver.calibrationOnUnknownDataset(dataPath, false, false, letter);
		DistortionCorrection correction = distortionCorrectionDriver.getDistortionCorrection();
		emDistortionDriver.calibrationOnUnknownDataset(dataPath, false, false, letter);
		ColumnVector pivot = emDistortionDriver.postVector();
		
		List<ColumnVector> emFiducials = this.computeEMFiducials(pivot, GFrames, correction);
		PointCloud emFiducialCloud = new PointCloud(emFiducials);
		if (printIntermediateSteps) {
			System.out.println("CT fiducials in EM coordinates after distortion correction for dataset " + letter);
			emFiducialCloud.asMatrix().print(0, 2);
		}
		
		Frame Freg = this.computeFRegistration(emFiducialCloud, ctFiducialData.getB());
		if (printIntermediateSteps) {
			System.out.println("F_reg for dataset " + letter);
			Freg.getRotationMatrix().print(0, 2);
			Freg.getTranslationVector().getMatrix().print(0, 2);
		}
		
		GFrames = emNavData.getGFrames();
		List<ColumnVector> ctNavs = this.transformEmNavigationToCt(pivot, GFrames, correction, Freg);
		PointCloud ctNavCloud = new PointCloud(ctNavs);
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-unknown-" + letter + "-output2.txt"), true));
				writer.println("EM navigation points in CT coordinates for dataset " + letter);
				ctNavCloud.asMatrix().print(writer, 0, 2);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

    }
	
	public static void main(String[] args) {
		EMFiducialDriver emFiducialDriver = new EMFiducialDriver();
		String dataPath = "data" + File.separator + "pa2" + File.separator;
		for (char letter = 'a'; letter <= 'f'; letter++) {
			emFiducialDriver.calibrationOnDebugDataset(dataPath, false, true, letter);
		}
		for (char letter = 'g'; letter <= 'j'; letter++) {
			emFiducialDriver.calibrationOnUnknownDataset(dataPath, false, true, letter);
		}
	}

}