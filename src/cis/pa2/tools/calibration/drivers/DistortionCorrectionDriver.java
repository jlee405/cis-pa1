package cis.pa2.tools.calibration.drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import Jama.Matrix;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.DistortionCalibration;
import cis.pa1.tools.data.CalibrationBodyData;
import cis.pa1.tools.data.CalibrationReadingsData;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.Output1Data;
import cis.pa2.tools.calibration.DistortionCorrection;

/**
 * A driver program to run a distortion pivot calibration
 * @author John Lee, Kyle Xiong
 *
 */
public final class DistortionCorrectionDriver implements DriverInterface {
	private List<DistortionCalibration> distortionCalibrations;
	private DistortionCorrection distortionCorrection;

	/**
	 * Performs a distortion calibration based on input paths 
	 * @param printStatements Whether to print while calibrating
	 * @param bodyPath The calbody data file pathname
	 * @param readingPath The calreadings data file pathname
	 */
	public void performDistortionCalibration(boolean printStatements, String bodyPath, String readingPath) {
		if (bodyPath == null) {
			bodyPath = "data" + File.separator + "pa1-debug-g-calbody.txt";	
		}
		if (readingPath == null) {
			readingPath = "data" + File.separator + "pa1-debug-g-calreadings.txt";	
		}
		DataFileParser parser = new DataFileParser();
		CalibrationBodyData bodyData = parser.parseCalBodyData(bodyPath);
		CalibrationReadingsData readingData = parser.parseCalReadingsData(readingPath);
		
		/*for (int i = 0; i < allpointcloudsD.size(); i++) {
			allpointcloudsD.get(i).asMatrix().print(0, 2);
		}*/
		setupParameters(printStatements, bodyData, readingData);
		correctDistortionCalibration(printStatements, readingData.getCFrames());

	}
	
	/**
	 * Sets up parameters and points.
	 */
	private void setupParameters(boolean printStatements, CalibrationBodyData bodyData, CalibrationReadingsData readingData) {
		distortionCalibrations = new ArrayList<>();
		List<Integer> params = readingData.getParameters();
		PointCloud dVectors = bodyData.getdCoordinates();
		PointCloud aVectors = bodyData.getaCoordinates();
		PointCloud cVectors = bodyData.getcCoordinates();
		List<PointCloud> DFrames = readingData.getDFrames();
		List<PointCloud> AFrames = readingData.getAFrames();
		List<PointCloud> CFrames = readingData.getCFrames();
		for (int i = 0; i < params.get(3); i++) {
			DistortionCalibration calibration = new DistortionCalibration(DFrames.get(i), dVectors,
					AFrames.get(i), aVectors,
					CFrames.get(i), cVectors);
			distortionCalibrations.add(calibration);
		}
		if (printStatements) {
			for (int i = 0; i < distortionCalibrations.size(); i++) {
				PointCloud expectedC = distortionCalibrations.get(i).expectedC();
				expectedC.asMatrix().print(0, 2);
			}
		}
	}
	
	/**
	 * Helper method to perform a distortion correction given a list of point cloud frames
	 * @param printStatements Whether to print progress to console
	 * @param CFrames The input set of frames
	 */
	private void correctDistortionCalibration(boolean printStatements, List<PointCloud> CFrames) {
		Matrix readC = PointCloud.pointCloudListToMatrix(CFrames);
		List<PointCloud> expectedCClouds = new ArrayList<>();
		for (int i = 0; i < this.distortionCalibrations.size(); i++) {
			expectedCClouds.add(this.distortionCalibrations.get(i).expectedC());
		}
		Matrix expectedC = PointCloud.pointCloudListToMatrix(expectedCClouds);
		if (printStatements) {
			expectedC.print(0, 2);
		}
		this.distortionCorrection = new DistortionCorrection(readC, expectedC, 5);
	}
	
	/**
	 * Returns the list of DistortionCalibration frames
	 * @return
	 */
	public List<DistortionCalibration> getDistortionCalibrations() {
		return this.distortionCalibrations;
	}
	
	/**
	 * Returns the distortion correction function for this driver
	 * @return
	 */
	public DistortionCorrection getDistortionCorrection() {
		return this.distortionCorrection;
	}
	
	@Override
	public void calibrationOnDebugDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		DataFileParser parser = new DataFileParser();
		
		String header = dataPath + "pa2-debug-";
		String bodyPath = header + letter + "-calbody.txt";
		String readingPath = header  + letter + "-calreadings.txt";
		Output1Data output = parser.parseOutput1Data(header + letter + "-output1.txt");
		
		this.performDistortionCalibration(false, bodyPath, readingPath);
		DistortionCorrection correction = this.getDistortionCorrection();

		List<DistortionCalibration> calculatedCFrames = this.getDistortionCalibrations();
		List<PointCloud> correctedPointClouds = PointCloud.matrixToPointCloudList(
				correction.getCorrectedMatrixTranspose(), calculatedCFrames.size());
		List<PointCloud> expectedCFrames = output.getCFrames();
		
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-debug-" + letter + "-output1.txt"), true));
				writer.println("Difference between calculated and output C Frames for dataset " + letter);
				for (int i = 0; i < calculatedCFrames.size(); i++) {
					Matrix correctedMatrix = correctedPointClouds.get(i).asMatrix();
					Matrix expectedMatrix = expectedCFrames.get(i).asMatrix();
					writer.println("Frame " + (i + 1));
					correctedMatrix.minus(expectedMatrix).print(writer, 0, 2);
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void calibrationOnUnknownDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		String header = dataPath + "pa2-unknown-";
		String bodyPath = header + letter + "-calbody.txt";
		String readingPath = header + letter + "-calreadings.txt";
		
		this.performDistortionCalibration(false, bodyPath, readingPath);
		DistortionCorrection correction = this.getDistortionCorrection();

		List<DistortionCalibration> calculatedCFrames = this.getDistortionCalibrations();
		List<PointCloud> correctedPointClouds = PointCloud.matrixToPointCloudList(
				correction.getCorrectedMatrixTranspose(), calculatedCFrames.size());
		
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-unknown-" + letter + "-output1.txt"), true));
				writer.println("Undistorted frames for unknown dataset " + letter);
				for (int i = 0; i < calculatedCFrames.size(); i++) {
					writer.println("Frame " + (i + 1));
					correctedPointClouds.get(i).asMatrix().print(writer, 0, 2);
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		DistortionCorrectionDriver driver = new DistortionCorrectionDriver();
		String dataPath = "data" + File.separator + "pa2" + File.separator;
		for (char letter = 'a'; letter <= 'f'; letter++) {
			driver.calibrationOnDebugDataset(dataPath, false, true, letter);
		} 
		
		for (char letter = 'g'; letter <= 'j'; letter++) {
			driver.calibrationOnUnknownDataset(dataPath, false, true, letter);
		}
	}
}
