package cis.pa2.tools.calibration.drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.drivers.EMCalibrationDriver;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.EMPivotData;
import cis.pa1.tools.data.Output1Data;
import cis.pa2.tools.calibration.DistortionCorrection;

/**
 * A driver program to run an EM pivot calibration after distortion correction
 * @author John Lee, Kyle Xiong
 *
 */
public final class EMDistortionCalibrationDriver implements DriverInterface {
	
	private DistortionCorrection correction;
	private ColumnVector post;
	private boolean performed = false;
	/**
	 * Performs an EM calibration based on input paths and correction coefficients 
	 * @param printStatements Whether to print while calibrating
	 * @param path The EM pivot data file pathname
	 */
	public ColumnVector performEMDistortionCalibration(boolean printStatements, String path, DistortionCorrection Ccorrection) {
		if (null == path) {
			path = "data" + File.separator + "pa2-debug-a-empivot.txt";
		}
		DataFileParser parser = new DataFileParser();
		EMPivotData data = parser.parseEMPivotData(path);
		List<PointCloud> GFrames = data.getGFrames();
		Matrix GFramesAsMatrix = PointCloud.pointCloudListToMatrix(GFrames);
		Matrix coefficients = Ccorrection.getBernsteinCoefficients();
		ColumnVector min = Ccorrection.getMinVector();
		ColumnVector max = Ccorrection.getMaxVector();
		this.correction = new DistortionCorrection(GFramesAsMatrix, 5, coefficients, min, max);
		EMCalibrationDriver driver = new EMCalibrationDriver();
		this.post = driver.pivotCalibration(printStatements, PointCloud.matrixToPointCloudList(
				this.correction.getCorrectedMatrixTranspose(), GFrames.size()));
		performed = true;
		return this.post;
	}
	
	public DistortionCorrection getDistortionCorrection() {
		return this.correction;
	}
	
	public ColumnVector postVector() {
		if (this.performed) {
			return this.post;
		} else {
			System.err.println("Driver has not calculated post vector yet.");
			System.exit(1);
			return null;
		}
	}
	
	@Override
	public void calibrationOnDebugDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		DistortionCorrectionDriver distortionCorrectionDriver = new DistortionCorrectionDriver();
	
		String debugHeader = "pa2-debug-";
		DataFileParser parser = new DataFileParser();
		String emPath = dataPath + debugHeader + letter + "-empivot.txt";
		String bodyPath = dataPath + debugHeader + letter + "-calbody.txt";
		String readingPath = dataPath + debugHeader + letter + "-calreadings.txt";
		
		distortionCorrectionDriver.performDistortionCalibration(printIntermediateSteps, bodyPath, readingPath);
		DistortionCorrection correction = distortionCorrectionDriver.getDistortionCorrection();
		ColumnVector calculatedPostVector = this.performEMDistortionCalibration(printIntermediateSteps, emPath, correction);
		
		Output1Data output =  parser.parseOutput1Data(dataPath + debugHeader + letter + "-output1.txt");
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-debug-" + letter + "-output1.txt"), true));
				writer.println("EM post vector after distortion correction for dataset " + letter);
			
				calculatedPostVector.getMatrix().print(writer, 0, 2);
			
				writer.println("Difference between undistorted and distorted EM post vectors for dataset " + letter);
				calculatedPostVector.minus(output.getEMPostVector()).getMatrix().print(writer, 0, 2);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void calibrationOnUnknownDataset(String dataPath, boolean printIntermediateSteps, boolean printFinal, char letter) {
		DistortionCorrectionDriver distortionCorrectionDriver = new DistortionCorrectionDriver();
		
		String unknownHeader = "pa2-unknown-";
		String emPath = dataPath + unknownHeader + letter + "-empivot.txt";
		String bodyPath = dataPath + unknownHeader + letter + "-calbody.txt";
		String readingPath = dataPath + unknownHeader + letter + "-calreadings.txt";
		
		distortionCorrectionDriver.performDistortionCalibration(false, bodyPath, readingPath);
		DistortionCorrection correction = distortionCorrectionDriver.getDistortionCorrection();
		ColumnVector calculatedPostVector = this.performEMDistortionCalibration(false, emPath, correction);
		
		if (printFinal) {
			try {
				PrintWriter writer = new PrintWriter(new FileWriter(new File("OUTPUT" + File.separator +
						"pa2-unknown-" + letter + "-output1.txt"), true));
				writer.println("EM post vector after distortion correction for dataset " + letter);
				calculatedPostVector.getMatrix().print(writer, 0, 2);
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		EMDistortionCalibrationDriver driver = new EMDistortionCalibrationDriver();
		String dataPath = "data" + File.separator + "pa2" + File.separator;
		for (char letter = 'a'; letter <= 'f'; letter++) {
			driver.calibrationOnDebugDataset(dataPath, false, true, letter);
		}
		for (char letter = 'g'; letter <= 'j'; letter++) {
			driver.calibrationOnUnknownDataset(dataPath, false, true, letter);
		}
	}
}
