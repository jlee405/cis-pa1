package cis.pa2.tools;

public class Polynomial {
	public static int choose(int m, int n) {
		long num = factorial(m);
		long denom = factorial(n)*factorial(m-n);
		return (int) (num/denom);
	}
	public static long factorial(int n) {
		long fact = 1;
		if (n > 0) {
			fact = n;
		} else if (n == 0) {
			fact = 1;
		} else {
			System.err.println("Factorial input must be 0 or higher.");
			System.exit(1);
		}
		for (int i = n - 1; i > 0; i--) {
			fact = fact * i;
		}
		return fact;
	}
	public static double binomialProbability(double x, int degree, int num) {
		/*if (x > 1 || x < 0) {
			throw new IllegalArgumentException("Probability input must be between 0 and 1");
		}*/
		double a = x;
		double b = 1 - x;
		double binomial = choose(degree, num)*Math.pow(a, num)*Math.pow(b, degree - num);
		return binomial;
	}
}
