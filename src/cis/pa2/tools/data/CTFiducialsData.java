package cis.pa2.tools.data;

import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.data.AbstractDataFileStorage;

/**
 * An object that stores information from a CT fiducials data file
 * @author John Lee, Kyle Xiong
 *
 */

public class CTFiducialsData extends AbstractDataFileStorage {

	PointCloud b;
	
	public PointCloud getB() {
		return b;
	}

	public void setB(PointCloud b) {
		this.b = b;
	}
	
	public CTFiducialsData() {
		this.b = new PointCloud(3);
	}
	
	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(1, parameters, "CT-FIDUCIALS");
		for (ColumnVector vector : vectors) {
			this.b.addPoint(vector);
		}
    }

}
