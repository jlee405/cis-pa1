package cis.pa2.tools.data;

import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.data.AbstractDataFileStorage;

/**
 * An object that stores information from an output2 data file
 * @author John Lee, Kyle Xiong
 *
 */
public class Output2Data extends AbstractDataFileStorage {
	
	private PointCloud v;

	public PointCloud getV() {
		return v;
	}

	public void setV(PointCloud v) {
		this.v = v;
	}
	
	public Output2Data() {
		this.v = new PointCloud(3);
	}

	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(1, parameters, "OUTPUT2");
		for (ColumnVector vector : vectors) {
			this.v.addPoint(vector);
		}
    }

}
