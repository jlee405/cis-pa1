package cis.pa2.tools.data;

import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.data.AbstractDataFileStorage;

/**
 * An object that stores information from an EM fiducials data file
 * @author John Lee, Kyle Xiong
 *
 */
public class EMFiducialsData extends AbstractDataFileStorage{
	
	private List<PointCloud> GFrames;
	
	public List<PointCloud> getGFrames() {
		return GFrames;
	}

	public void setGFrames(List<PointCloud> gFrames) {
		GFrames = gFrames;
	}

	public EMFiducialsData() {
		this.GFrames = new ArrayList<>();
	}

	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(2, parameters, "EM-FIDUCIALSS");
		int NG = parameters.get(0);
		int NB = parameters.get(1);
		for (int i = 0; i < NB; i++) {
			PointCloud GCoordinates = new PointCloud(3);
			for (int j = 0; j < NG; j++) {
				GCoordinates.addPoint(vectors.get(i * NG + j));
			}
			GFrames.add(GCoordinates);
		}
		
    }
	
}
