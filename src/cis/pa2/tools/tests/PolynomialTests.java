package cis.pa2.tools.tests;

import org.junit.Assert;
import org.junit.Test;

import cis.pa2.tools.Polynomial;

public class PolynomialTests {

	@Test
	public void factorialTest() {
		long test = new Long("2432902008176640000");
		Assert.assertEquals(test, Polynomial.factorial(20));
	}
	
	@Test
	public void chooseTest() {
		Assert.assertEquals(77520, Polynomial.choose(20, 7));
		Assert.assertEquals(1, Polynomial.choose(5, 0));
	}
	
	@Test
	public void binomialProbabilityTest() {
		Assert.assertEquals(0.2592, Polynomial.binomialProbability(0.4,5,1), 10e-5);

	}
}
