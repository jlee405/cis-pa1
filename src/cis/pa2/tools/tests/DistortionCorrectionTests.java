package cis.pa2.tools.tests;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.DistortionCalibration;
import cis.pa1.tools.tests.TestHelper;
import cis.pa2.tools.calibration.DistortionCorrection;
import cis.pa2.tools.calibration.drivers.DistortionCorrectionDriver;

public class DistortionCorrectionTests {

	
	/**
	 * Performs a distortion correction, then 
	 * verifies that applying the correction 
	 * function to the original dataset provides 
	 * the same matrix as the corrected matrix
	 */
	@Test
	public void verifyCorrectionFunction() {
		DistortionCorrectionDriver driver = new DistortionCorrectionDriver();
		String bodyPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-a-calbody.txt";
		String readingPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-a-calreadings.txt";
		
		driver.performDistortionCalibration(false, bodyPath, readingPath);
		List<DistortionCalibration> calculatedCFrames = driver.getDistortionCalibrations();
		DistortionCorrection correction = driver.getDistortionCorrection();
		List<PointCloud> originalCFrames = new ArrayList<>();
		for (int i = 0; i < calculatedCFrames.size(); i++) {
			originalCFrames.add(calculatedCFrames.get(i).cloudC());
		}
		DistortionCorrection verification = new DistortionCorrection(
				PointCloud.pointCloudListToMatrix(originalCFrames), 5, 
				correction.getBernsteinCoefficients(), correction.getMinVector(), correction.getMaxVector());
		
		List<PointCloud> expectedCFrames = PointCloud.matrixToPointCloudList(
				correction.getCorrectedMatrixTranspose(), originalCFrames.size());
		List<PointCloud> correctedCFrames = PointCloud.matrixToPointCloudList(
				verification.getCorrectedMatrixTranspose(), originalCFrames.size());
		
		for (int i = 0; i < calculatedCFrames.size(); i++) {
			TestHelper.assertMatricesMatch(
					expectedCFrames.get(i).asMatrix(), correctedCFrames.get(i).asMatrix(), 10e-3);
		}
	}
}
