package cis.pa2.tools;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;

/**
 * Class to calculate the Bernstein 5th-order polynomial of an input vector
 * or an input double.
 * @author turke_000
 *
 */
public class Bernstein {
	public static Matrix polynomial(ColumnVector input, int degree, int[] num) {
		Matrix inputm = input.getMatrix();
		Matrix m = new Matrix(inputm.getRowDimension(), inputm.getColumnDimension());
		for (int i = 0; i < m.getRowDimension(); i++) {
			m.set(i, 0, Polynomial.binomialProbability(inputm.get(i, 0), degree, num[i]));
		}
		return m;
	}
	public static double polynomial(double input, int degree, int num) {
		double d = Polynomial.binomialProbability(input, degree, num);
		return d;
	}
}