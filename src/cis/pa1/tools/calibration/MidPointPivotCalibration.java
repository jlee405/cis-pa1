package cis.pa1.tools.calibration;
import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.HornRegistration;
import cis.pa1.tools.PointCloud;
/**
 * Pivot calibration class that will return primarily a rotation matrix and
 * transformation column vector.
 * @author Kyle Xiong, John Lee
 *
 */
public class MidPointPivotCalibration {
	private PointCloud allVectors;
	private Matrix rotationMatrix;
	private PointCloud radialVectors;
	private ColumnVector midVector, translationVector;
	/**
	 * Calibration constructor that calibrates a pivot calibration.
	 * @param pc the input point cloud to calibrate
	 */
	public MidPointPivotCalibration(PointCloud pc, MidPointPivotCalibration radialpc) {
		this.allVectors = pc; 
		this.midVector = this.calculateMidVector();
		this.radialVectors = this.getRadial(radialpc); 
		Matrix radialreferencecloud = this.getRadialReferenceCloud(radialpc);
		HornRegistration registration = new HornRegistration(radialreferencecloud, this.allVectors.asMatrix(), true);
		this.rotationMatrix = registration.rotationMatrix();
		this.translationVector = registration.transVector();
	}
	/**
	 * Computes the midpoint vector taking the average of all marker vectors' coordinates.
	 * @return the midpoint vector as a matrix
	 */
	private ColumnVector calculateMidVector() {
		int dim = this.allVectors.getDimension();
		int numVectors = this.allVectors.getSize();
		ColumnVector mid = new ColumnVector(dim);
		for (int i = 0; i < numVectors; i++) {
			mid = mid.plus(this.allVectors.get(i));
		}
		mid = mid.times(1.0/numVectors); // average vector
		return mid;
	}
	/**
	 * Computes the vectors from the center of the pivot object to each marker.
	 * @return a point cloud of vectors from center to each marker
	 */
	private PointCloud getRadial(MidPointPivotCalibration pc) {
		if (pc == null) {
			pc = this;
		}
		int dim = this.allVectors.getDimension();
		int numVectors = this.allVectors.getSize();
		PointCloud radialCloud = new PointCloud(dim);
		for (int i = 0; i < numVectors; i++) {
			radialCloud.addPoint(this.allVectors.get(i).minus(pc.midVector));
		}
		return radialCloud;
	}
	/**
	 * Get the midpoint vector
	 * @return the midpoint vector
	 */
	public ColumnVector midVector() {
		return this.midVector;
	}
	public PointCloud allPoints() {
		return this.allVectors;
	}
	public Matrix getRadialReferenceCloud(MidPointPivotCalibration pc) {
		if (pc == null) {
			pc = this;
		}
		return pc.radialVectors();
	}
	/**
	 * Public method that gets the pivot vector matrix.
	 * @return the matrix of vectors from each marker to the pivot point
	 */
	public Matrix radialVectors() {
		return this.radialVectors.asMatrix();
	}
	/**
	 * Public method that gets the midpoint vector
	 * @return the midpoint vector as a matrix
	 */
	public Matrix midPoint() {
		return this.midVector.getMatrix();
	}
	/**
	 * Public method that gets the midpoint vector
	 * @return the midpoint vector as a ColumnVector
	 */
	public ColumnVector midPointVector() {
		return new ColumnVector(this.midPoint());
	}
	/**
	 * Gets the rotation matrix from the tracker to the markers.
	 * @return the rotation matrix
	 */
	public Matrix rotationMatrix() {
		return this.rotationMatrix;
	}
	/**
	 * Gets the translation vector from the tracker to the markers.
	 * @return the translation vector as a ColumnVector.
	 */
	public ColumnVector translationVector() {
		return this.translationVector;
	}
}