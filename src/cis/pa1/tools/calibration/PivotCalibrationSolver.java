package cis.pa1.tools.calibration;
import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import Jama.Matrix;

/**
 * This class will store a number of pivot calibration results and process them
 * to get the P_pivot and P_t vectors.
 * @author Kyle Xiong, John Lee
 *
 */
public class PivotCalibrationSolver {
	private List<MidPointPivotCalibration> allpc;
	public PivotCalibrationSolver() {
		this.allpc = new ArrayList<>();
	}
	private boolean unique(MidPointPivotCalibration pc) {
		Matrix rot = pc.rotationMatrix();
		ColumnVector trans = pc.translationVector();
		for (int i = 0; i < this.allpc.size(); i++) {
			if (rot.equals(allpc.get(i).rotationMatrix()) && trans.equals(allpc.get(i).translationVector())) {
				return false;
			}
		}
		return true;
	}
	public MidPointPivotCalibration addPC(MidPointPivotCalibration pc) {
		if (this.unique(pc)) {
			this.allpc.add(pc);
		}
		return pc;
	}
	/**
	 * Left matrix in the equation Ab = c. Its format is: [R|-I] where R is the matrix of all rotation
	 * matrices and -I is the negative identity matrix.
	 * @return the left matrix
	 */
	public Matrix getLeftMatrix() {
		int rotrownum = this.allpc.get(0).rotationMatrix().getRowDimension(); // num of rows in rot matrix, rrn
		int rotcolnum = this.allpc.get(0).rotationMatrix().getColumnDimension(); // num of columns, rcn
		int leftrownum = rotrownum * this.allpc.size(); // total number of rows in left matrix
		int leftcolnum = rotcolnum * 2; // total number of columns in left matrix
		Matrix identity = Matrix.identity(rotrownum, rotcolnum);
		Matrix left = new Matrix(leftrownum, leftcolnum);
		for (int i = 0; i < this.allpc.size(); i++) {
			// set left matrix to be [R|-I]
			left.setMatrix(rotrownum*i, rotrownum*(i + 1) - 1, 0, rotcolnum - 1, this.allpc.get(i).rotationMatrix());
			left.setMatrix(rotrownum*i, rotrownum*(i + 1) - 1, rotcolnum, leftcolnum - 1, identity.times(-1));
		}
		return left;
	}
	/**
	 * Right matrix in the equation Ab = c. Its format is: [p] where p is the column vector of all
	 * translations stacked on top of each other: [p] = [p1; p2; p3; ...]
	 * @return the right matrix
	 */
	public Matrix getRightMatrix() {
		int transrownum = this.allpc.get(0).translationVector().getMatrix().getRowDimension(); // num of rows in trans matrix, trn
		int transcolnum = this.allpc.get(0).translationVector().getMatrix().getColumnDimension(); // num of columns, tcn
		int rightrownum = transrownum * this.allpc.size(); // total number of rows in right matrix
		int rightcolnum = transcolnum; // total number of columns in right matrix
		Matrix right = new Matrix(rightrownum, rightcolnum);
		for (int i = 0; i < this.allpc.size(); i++ ) {
			ColumnVector cv = this.allpc.get(i).translationVector();
			right.setMatrix(transrownum*i, transrownum*(i + 1) - 1, 0, rightcolnum - 1, cv.getMatrix());
		}
		right = right.times(-1);
		return right;
	}
	/**
	 * Pseudo-inverse is: Left^-1*Right
	 * @return the solution of the pseudo-inverse
	 */
	public Matrix solution() {
		return this.getLeftMatrix().solve(this.getRightMatrix());
	}
}