package cis.pa1.tools.calibration.drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.MidPointPivotCalibration;
import cis.pa1.tools.calibration.PivotCalibrationSolver;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.EMPivotData;
import cis.pa1.tools.data.Output1Data;

/**
 * A driver program to run an EM pivot calibration
 * @author John Lee, Kyle Xiong
 *
 */
public final class EMCalibrationDriver {
	
	/**
	 * Begin pivot calibration after setting up params and points.
	 */
	public ColumnVector pivotCalibration(boolean printStatements, EMPivotData data) {
		List<PointCloud> GFrames = data.getGFrames();
		return pivotCalibration(printStatements, GFrames);
		
	}
	
	public ColumnVector pivotCalibration(boolean printStatements, List<PointCloud> GFrames) {
		PivotCalibrationSolver solver = new PivotCalibrationSolver(); // get new processor
		// add all pivot calibrations for each frame to the processor
		MidPointPivotCalibration initpc = new MidPointPivotCalibration(GFrames.get(0), null);

		for (int i = 1 ; i < GFrames.size(); i++) {
			MidPointPivotCalibration pc;
			pc = new MidPointPivotCalibration(GFrames.get(i), initpc);
			solver.addPC(pc);
		}
		// get pseudo-inverse solution after all frames are added.
		Matrix PI = solver.solution();
		if (printStatements) {
			System.out.println("EM Post Postion:");
			PI.getMatrix(3, 5, 0, 0).print(0, 3);
		}
		return new ColumnVector(PI.getMatrix(3, 5, 0, 0));
	}
	
	/**
	 * Performs an EM calibration based on input paths 
	 * @param printStatements Whether to print while calibrating
	 * @param path The EM pivot data file pathname
	 */
	public ColumnVector performEMCalibration(boolean printStatements, String path) {
		if (null == path) {
			path = "data" + File.separator + "pa1-debug-a-empivot.txt";
		}
		DataFileParser parser = new DataFileParser();
		EMPivotData data = parser.parseEMPivotData(path);
		return pivotCalibration(printStatements, data);
	}
	
	public static void main(String[] args) {
		EMCalibrationDriver driver = new EMCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		try {
			PrintWriter writer = new PrintWriter(new File("EMcalibration.txt"));
			for (char letter = 'a'; letter <= 'g'; letter++) {
				String path = "data" + File.separator + "pa1-debug-" + letter + "-empivot.txt";
				ColumnVector calculatedPostVector = driver.performEMCalibration(false, path);
				PointCloud postVectors = new PointCloud(3);
				postVectors.addPoint(calculatedPostVector);
				Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa1-debug-" + letter + "-output1.txt");
				postVectors.addPoint(output.getEMPostVector());
				
				writer.println("EM post vector for debug dataset " + letter + " vs. expected");
				postVectors.asMatrix().print(writer, 0, 2);			
			}
			for (char letter = 'h'; letter <= 'k'; letter++) {
				String path = "data" + File.separator + "pa1-unknown-" + letter + "-empivot.txt";
				ColumnVector postVector = driver.performEMCalibration(false, path);
				writer.println("EM post vector for unknown dataset " + letter);
				postVector.getMatrix().print(writer, 0, 2);
			}
			writer.close();
		} catch (FileNotFoundException e){
			e.printStackTrace();
		}
	}
}
