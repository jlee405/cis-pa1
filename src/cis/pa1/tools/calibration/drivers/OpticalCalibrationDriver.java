package cis.pa1.tools.calibration.drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.Frame;
import cis.pa1.tools.HornRegistration;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.MidPointPivotCalibration;
import cis.pa1.tools.calibration.PivotCalibrationSolver;
import cis.pa1.tools.data.CalibrationBodyData;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.OpticalPivotData;
import cis.pa1.tools.data.Output1Data;

/**
 * A driver program to run an optical pivot calibration
 * @author John Lee, Kyle Xiong
 *
 */
public final class OpticalCalibrationDriver {
	
	private ColumnVector pivotCalibration(boolean printStatements, OpticalPivotData optData, CalibrationBodyData bodyData) {
		PointCloud dCloud = bodyData.getdCoordinates();
		List<PointCloud> DFrames = optData.getDFrames();
		List<PointCloud> HFrames = optData.getHFrames();
		MidPointPivotCalibration initialCalibration = null;
		PivotCalibrationSolver solver = new PivotCalibrationSolver(); // get new processor

		for (int i = 0; i < DFrames.size(); i++) {
			HornRegistration hr = new HornRegistration (DFrames.get(i).asMatrix(), dCloud.asMatrix(), true);
			PointCloud HiTransformed = HFrames.get(i);
			Frame frame = new Frame(hr.rotationMatrix(), hr.transVector());
			HiTransformed.frameShift(frame);
			HFrames.set(i, HiTransformed);
			MidPointPivotCalibration pc;
			if (i == 0) {
				initialCalibration = new MidPointPivotCalibration(HFrames.get(i), null);
				pc = initialCalibration;
			}
			else {
				pc = new MidPointPivotCalibration(HFrames.get(i), initialCalibration);
			}
			solver.addPC(pc);
		}
		
		Matrix PI = solver.solution();
		if (printStatements) {
			System.out.println("EM Post Postion:");
			PI.getMatrix(3, 5, 0, 0).print(0, 3);
		}
		return new ColumnVector(PI.getMatrix(3, 5, 0, 0));
	}
		
	/**
	 * Performs an optical calibration based on input paths 
	 * @param printStatements Whether to print while calibrating
	 * @param optPath The optical pivot data file pathname
	 * @param bodyPath The calbody data file pathname
	 */
	public ColumnVector performOpticalCalibration(boolean printStatements, String optPath, String bodyPath) {
		if (null == optPath) {
			optPath = "data" + File.separator + "pa1-debug-g-optpivot.txt";
		}
		
		if (null == bodyPath) {
			bodyPath = "data" + File.separator + "pa1-debug-g-calbody.txt";
		}
		DataFileParser parser = new DataFileParser();
		CalibrationBodyData bodyData = parser.parseCalBodyData(bodyPath);
		OpticalPivotData optData = parser.parseOptPivotData(optPath);
		return pivotCalibration(printStatements, optData, bodyData);
	}
	
	public static void main(String[] args) {
		OpticalCalibrationDriver driver = new OpticalCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		try {
			PrintWriter writer = new PrintWriter(new File("opticalcalibration.txt"));
			for (char letter = 'a'; letter <= 'g'; letter++) {
				String optPath = "data" + File.separator + "pa1-debug-" + letter + "-optpivot.txt";
				String bodyPath = "data" + File.separator + "pa1-debug-" + letter + "-calbody.txt";
				ColumnVector calculatedPostVector = driver.performOpticalCalibration(false, optPath, bodyPath);
				PointCloud postVectors = new PointCloud(3);
				postVectors.addPoint(calculatedPostVector);
				Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa1-debug-" + letter + "-output1.txt");
				postVectors.addPoint(output.getOpticalPostVector());
				
				writer.println("Optical post vector for debug dataset " + letter + " vs. expected");
				postVectors.asMatrix().print(writer, 0, 2);			
			}
			for (char letter = 'h'; letter <= 'k'; letter++) {
				String optPath = "data" + File.separator + "pa1-unknown-" + letter + "-optpivot.txt";
				String bodyPath = "data" + File.separator + "pa1-unknown-" + letter + "-calbody.txt";
				ColumnVector postVector = driver.performOpticalCalibration(false, optPath, bodyPath);
				writer.println("Optical post vector for unknown dataset " + letter);
				postVector.getMatrix().print(writer, 0, 2);
			}
			writer.close();
		} catch (FileNotFoundException e) {
			
		}
	}
}
