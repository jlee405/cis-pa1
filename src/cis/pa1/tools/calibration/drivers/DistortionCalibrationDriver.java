package cis.pa1.tools.calibration.drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.DistortionCalibration;
import cis.pa1.tools.data.CalibrationBodyData;
import cis.pa1.tools.data.CalibrationReadingsData;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.Output1Data;

/**
 * A driver program to run a distortion pivot calibration
 * @author John Lee, Kyle Xiong
 *
 */
public final class DistortionCalibrationDriver { 
	private List<DistortionCalibration> distortionCalibrations;
	/**
	 * Begin pivot calibration after setting up params and points.
	 */
	private void distortCalibration(boolean printStatements, CalibrationBodyData bodyData, CalibrationReadingsData readingData) {
		distortionCalibrations = new ArrayList<>();
		List<Integer> params = readingData.getParameters();
		PointCloud dVectors = bodyData.getdCoordinates();
		PointCloud aVectors = bodyData.getaCoordinates();
		PointCloud cVectors = bodyData.getcCoordinates();
		List<PointCloud> DFrames = readingData.getDFrames();
		List<PointCloud> AFrames = readingData.getAFrames();
		List<PointCloud> CFrames = readingData.getCFrames();
		for (int i = 0; i < params.get(3); i++) {
			DistortionCalibration calibration = new DistortionCalibration(DFrames.get(i), dVectors,
					AFrames.get(i), aVectors,
					CFrames.get(i), cVectors);
			distortionCalibrations.add(calibration);
		}
		for (int i = 0; i < distortionCalibrations.size(); i++) {
			PointCloud expectedC = distortionCalibrations.get(i).expectedC();
			if (printStatements) {
				expectedC.asMatrix().print(0, 2);
			}
		}
	}
	
	/**
	 * Performs a distortion calibration based on input paths 
	 * @param printStatements Whether to print while calibrating
	 * @param bodyPath The calbody data file pathname
	 * @param readingPath The calreadings data file pathname
	 */
	public void performDistortionCalibration(boolean printStatements, String bodyPath, String readingPath) {
		if (bodyPath == null) {
			bodyPath = "data" + File.separator + "pa1-debug-g-calbody.txt";	
		}
		if (readingPath == null) {
			readingPath = "data" + File.separator + "pa1-debug-g-calreadings.txt";	
		}
		DataFileParser parser = new DataFileParser();
		CalibrationBodyData bodyData = parser.parseCalBodyData(bodyPath);
		CalibrationReadingsData readingData = parser.parseCalReadingsData(readingPath);
		
		/*for (int i = 0; i < allpointcloudsD.size(); i++) {
			allpointcloudsD.get(i).asMatrix().print(0, 2);
		}*/
		distortCalibration(printStatements, bodyData, readingData);
	}
	
	/**
	 * Returns the list of DistortionCalibration frames
	 * @return
	 */
	public List<DistortionCalibration> getDistortionCalibrations() {
		return this.distortionCalibrations;
	}
	
	public static void main(String[] args) {
		DistortionCalibrationDriver driver = new DistortionCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		try {
			PrintWriter writer = new PrintWriter(new File("distortioncalibration.txt"));
			for (char letter = 'a'; letter <= 'f'; letter++) {
				//String bodyPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calbody.txt";
				//String readingPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calreadings.txt";
				String bodyPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calbody.txt";
				String readingPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calreadings.txt";
				driver.performDistortionCalibration(false, bodyPath, readingPath);
				List<DistortionCalibration> calculatedCFrames = driver.getDistortionCalibrations();
				Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-output1.txt");
				List<PointCloud> expectedCFrames = output.getCFrames();
				writer.println("Distortion calibration for debug dataset " + letter + " vs. expected");
				for (int i = 0; i < calculatedCFrames.size(); i++) {				
					writer.println("Frame " + i);
					calculatedCFrames.get(i).expectedC().asMatrix().print(writer, 0, 2);
					expectedCFrames.get(i).asMatrix().print(writer, 0, 2);
				}
			}
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		/*
		for (char letter = 'f'; letter <= 'f'; letter++) {
			//String bodyPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calbody.txt";
			//String readingPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calreadings.txt";
			String bodyPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calbody.txt";
			String readingPath = "data" + File.separator + "pa2" + File.separator + "pa2-debug-" + letter + "-calreadings.txt";
			driver.performDistortionCalibration(false, bodyPath, readingPath);
			List<DistortionCalibration> calculatedCFrames = driver.getDistortionCalibrations();
			
			System.out.println("Distortion calibration for unknown dataset " + letter);
			for (int i = 0; i < calculatedCFrames.size(); i++) {
				System.out.println("Frame " + i);
				calculatedCFrames.get(i).expectedC().asMatrix().print(0, 3);
			}
		}*/
	}
}