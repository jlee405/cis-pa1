package cis.pa1.tools.calibration;
import cis.pa1.tools.Frame;
import cis.pa1.tools.HornRegistration;
import cis.pa1.tools.PointCloud;
/**
 * An object that stores information about a distortion calibration frame
 * @author John Lee, Kyle Xiong
 *
 */
public class DistortionCalibration {
	private PointCloud DCloud, dCloud, ACloud, aCloud, CCloud, cCloud;
	public DistortionCalibration(PointCloud D, PointCloud d, PointCloud A, PointCloud a, PointCloud C, PointCloud c) {
		this.DCloud = D;
		this.dCloud = d;
		this.ACloud = A;
		this.aCloud = a;
		this.CCloud = C;
		this.cCloud = c;
	}
	public Frame frameD() {
		HornRegistration hr = new HornRegistration (dCloud.asMatrix(), DCloud.asMatrix(), true);
		return new Frame(hr.rotationMatrix().times(hr.scaleFactor()), hr.transVector());
	}
	public Frame frameA() {
		HornRegistration hr = new HornRegistration (aCloud.asMatrix(), ACloud.asMatrix(), true);
		return new Frame(hr.rotationMatrix().times(hr.scaleFactor()), hr.transVector());
	}
	public Frame frameC() {
		HornRegistration hr = new HornRegistration (cCloud.asMatrix(), CCloud.asMatrix(), true);
		return new Frame(hr.rotationMatrix().times(hr.scaleFactor()), hr.transVector());
	}
	
	public PointCloud cloudD() {
		return this.DCloud;
	}
	
	public PointCloud cloudd() {
		return this.dCloud;
	}
	
	public PointCloud cloudA() {
		return this.ACloud;
	}
	
	public PointCloud clouda() {
		return this.aCloud;
	}
	
	public PointCloud cloudC() {
		return this.CCloud;
	}
	
	public PointCloud cloudc() {
		return this.cCloud;
	}
	
	public Frame expectedFrameC() {
		Frame D = this.frameD().inverse();
		Frame A = this.frameA();
		return D.combineFrames(A);
	}
	public PointCloud expectedC() {
		PointCloud expectedCCloud = cCloud.clone();
		expectedCCloud.frameShift(this.expectedFrameC());
		return expectedCCloud;
	}
}