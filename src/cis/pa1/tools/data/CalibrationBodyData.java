package cis.pa1.tools.data;

import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;

/**
 * An object that stores information from a calibration body data file
 * @author John Lee, Kyle Xiong
 *
 */
public class CalibrationBodyData extends AbstractDataFileStorage {
	private PointCloud dCoordinates;
	private PointCloud aCoordinates;
	private PointCloud cCoordinates;
	
	public CalibrationBodyData() {
		this.dCoordinates = new PointCloud(3);
		this.aCoordinates = new PointCloud(3);
		this.cCoordinates = new PointCloud(3);

	}
	
	public PointCloud getdCoordinates() {
		return dCoordinates;
	}

	public void setdCoordinates(PointCloud dCoordinates) {
		this.dCoordinates = dCoordinates;
	}

	public PointCloud getaCoordinates() {
		return aCoordinates;
	}

	public void setaCoordinates(PointCloud aCoordinates) {
		this.aCoordinates = aCoordinates;
	}

	public PointCloud getcCoordinates() {
		return cCoordinates;
	}

	public void setcCoordinates(PointCloud cCoordinates) {
		this.cCoordinates = cCoordinates;
	}
	
	@Override
	public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(3, parameters, "CALBODY");
		this.parameters = parameters;
		int nD = parameters.get(0);
		int nA = parameters.get(1);
		int nC = parameters.get(2);
		
		for (int i = 0; i < nD; i++) {
			this.dCoordinates.addPoint(vectors.get(i));
		}
		
		for (int j = 0; j < nA; j++) {
			this.aCoordinates.addPoint(vectors.get(j + nD));
		}
		
		for (int k = 0; k < nC; k++) {
			this.cCoordinates.addPoint(vectors.get(k + nD + nA));
		}
	}
}
