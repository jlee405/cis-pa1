package cis.pa1.tools.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa2.tools.data.CTFiducialsData;
import cis.pa2.tools.data.EMFiducialsData;
import cis.pa2.tools.data.EMNavigationData;
import cis.pa2.tools.data.Output2Data;

/**
 * A utility class that parses each of the different kinds of data files
 * @author John Lee, Kyle Xiong
 *
 */
public class DataFileParser {
	private List<Integer> readParameters(BufferedReader br) {
		String line;
		List<Integer> params = new ArrayList<>();
		try {
			line = br.readLine();
			String[] input = line.split(", ");
			for (int i = 0; i < input.length; i++) {
				if (isInteger(input[i])) {
					params.add(Integer.parseInt(input[i]));
				}
			}
			if (params.isEmpty()) {
				input = line.split(",");
				for (int i = 0; i < input.length; i++) {
					if (isInteger(input[i])) {
						params.add(Integer.parseInt(input[i]));
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return params;
		
	}
	
	private List<ColumnVector> convertToColumnVectors(List<List<Double>> coordinateList) {
		List<ColumnVector> columnVectorList = new ArrayList<>();
		for (int i = 0; i < coordinateList.size(); i++) {
			double[] vecarray = new double[coordinateList.get(i).size()];
			for (int j = 0; j < vecarray.length; j++) {
				vecarray[j] = coordinateList.get(i).get(j);
			}
			columnVectorList.add(new ColumnVector(vecarray));
		}		
		return columnVectorList;
	}
	
	private List<List<Double>> readPoints(BufferedReader br) {
		
		List<List<Double>> coordinateList = new ArrayList<>();
		String line;
		try {
			while ((line = br.readLine()) != null) {
				String[] input = line.split(", ");
				ArrayList<Double> tempcoords = new ArrayList<Double>();
				for (int i = 0; i < input.length; i++) {
					if (isDouble(input[i])) {
						tempcoords.add(Double.parseDouble(input[i]));
					}
				}
				coordinateList.add(tempcoords);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return coordinateList;
		
	}
	
	private void parseDataFile(DataFileStorage data, String path) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			List<Integer> parameters = this.readParameters(br);
			List<ColumnVector> vectors = this.convertToColumnVectors(this.readPoints(br));
			data.columnVectorsToPointCloud(vectors, parameters);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Parses a calbody data file
	 * @param path The name of the file
	 * @return A CalibrationBodyData data structure
	 */
	public CalibrationBodyData parseCalBodyData(String path) {
		CalibrationBodyData data = new CalibrationBodyData();
		this.parseDataFile(data, path);
		return data;
	}
	
	/**
	 * Parses a calreadings data file
	 * @param path The name of the file
	 * @return A CalibrationReadingsData data structure
	 */
	public CalibrationReadingsData parseCalReadingsData(String path) {
		CalibrationReadingsData data = new CalibrationReadingsData();
		this.parseDataFile(data, path);
		return data;	
	}
	
	/**
	 * Parses a empivot data file
	 * @param path The name of the file
	 * @return A EMPivotData data structure
	 */
	public EMPivotData parseEMPivotData(String path) {
		EMPivotData data = new EMPivotData();
		this.parseDataFile(data, path);
		return data;	
	}
	
	/**
	 * Parses a optpivot data file
	 * @param path The name of the file
	 * @return A OpticalPivotData data structure
	 */
	public OpticalPivotData parseOptPivotData(String path) {
		OpticalPivotData data = new OpticalPivotData();
		this.parseDataFile(data, path);
		return data;	
	}
	
	/**
	 * Parses an output1 data file
	 * @param path The name of the file
	 * @return A Output1Data data structure
	 */
	public Output1Data parseOutput1Data(String path) {
		Output1Data data = new Output1Data();
		this.parseDataFile(data, path);
		return data;
	}	
	
	/**
	 * Parses a CT fiducials data file
	 * @param path The name of the file
	 * @return A CTFiducialsData data structure
	 */	
	public CTFiducialsData parseCTFiducialsData(String path) {
		CTFiducialsData data = new CTFiducialsData();
		this.parseDataFile(data, path);
		return data;
	}
	
	/**
	 * Parses a EM fiducials data file
	 * @param path The name of the file
	 * @return A EMFiducialsData data structure
	 */	
	public EMFiducialsData parseEMFiducialsData(String path) {
		EMFiducialsData data = new EMFiducialsData();
		this.parseDataFile(data, path);
		return data;
	}
	
	/**
	 * Parses an EM navigation data file
	 * @param path The name of the file
	 * @return A EMNavigationData data structure
	 */
	public EMNavigationData parseEMNavData(String path) {
		EMNavigationData data = new EMNavigationData();
		this.parseDataFile(data, path);
		return data;
	}
	
	/**
	 * Parses an output2 data file
	 * @param path The name of the file
	 * @return A Output2Data data structure
	 */
	public Output2Data parseOutput2Data(String path) {
		Output2Data data = new Output2Data();
		this.parseDataFile(data, path);
		return data;
	}
	
	/**
	 * Checks if a string input is an integer
	 * @param str string input
	 * @return if string input is an integer
	 */
	private boolean isInteger(String str) {
		try {Integer.parseInt(str);} catch(NumberFormatException e) {return false;}
		return true;
	}
	/**
	 * Checks if a string input is a double
	 * @param str string input
	 * @return if string input is a double
	 */
	private boolean isDouble(String str) {
		try {Double.parseDouble(str);} catch(NumberFormatException e) {return false;}
		return true;
	} 
}
