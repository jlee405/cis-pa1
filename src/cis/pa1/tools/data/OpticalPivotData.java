package cis.pa1.tools.data;

import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;

/**
 * An object that stores information from an optical pivot data file
 * @author John Lee, Kyle Xiong
 *
 */
public class OpticalPivotData extends AbstractDataFileStorage{
	private List<PointCloud> DFrames;
	private List<PointCloud> HFrames;
	
	
	public List<PointCloud> getDFrames() {
		return DFrames;
	}

	public void setDFrames(List<PointCloud> dFrames) {
		DFrames = dFrames;
	}

	public List<PointCloud> getHFrames() {
		return HFrames;
	}

	public void setHFrames(List<PointCloud> hFrames) {
		HFrames = hFrames;
	}

	public OpticalPivotData() {
		this.DFrames = new ArrayList<>();
		this.HFrames = new ArrayList<>();
	}

	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(3, parameters, "OPTPIVOT");
		this.parameters = parameters;
		int ND = parameters.get(0);
		int NH = parameters.get(1);
		int NFrames = parameters.get(2);
		int combinedParameters = ND + NH;
		
		for (int i = 0; i < NFrames; i++) {
			PointCloud DCoordinates = new PointCloud(3);
			PointCloud HCoordinates = new PointCloud(3);
			for (int j = 0; j < ND; j++) {
				DCoordinates.addPoint(vectors.get(combinedParameters * i + j));
			}
			for (int j = 0; j < NH; j++) {
				HCoordinates.addPoint(vectors.get(combinedParameters * i + ND + j));
			}
			DFrames.add(DCoordinates);
			HFrames.add(HCoordinates);

		}	    
    }

}
