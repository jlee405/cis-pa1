package cis.pa1.tools.data;
import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;

/**
 * An object that stores information from a calibration readings data file
 * @author John Lee, Kyle Xiong
 *
 */
public class CalibrationReadingsData extends AbstractDataFileStorage {
	
	private List<PointCloud> DFrames;
	private List<PointCloud> AFrames;
	private List<PointCloud> CFrames;
	
	public CalibrationReadingsData() {
		this.DFrames = new ArrayList<>();
		this.AFrames = new ArrayList<>();
		this.CFrames = new ArrayList<>();
	}
	
	public List<PointCloud> getDFrames() {
		return DFrames;
	}



	public void setDFrames(List<PointCloud> dFrames) {
		DFrames = dFrames;
	}



	public List<PointCloud> getAFrames() {
		return AFrames;
	}



	public void setAFrames(List<PointCloud> aFrames) {
		AFrames = aFrames;
	}



	public List<PointCloud> getCFrames() {
		return CFrames;
	}



	public void setCFrames(List<PointCloud> cFrames) {
		CFrames = cFrames;
	}


	

	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(4, parameters, "CALREADINGS");
		this.parameters = parameters;
		int ND = parameters.get(0);
		int NA = parameters.get(1);
		int NC = parameters.get(2);
		int NFrames = parameters.get(3);
		int combinedParameters = ND + NA + NC;
		
		for (int i = 0; i < NFrames; i++) {
			PointCloud DCoordinates = new PointCloud(3);
			PointCloud ACoordinates = new PointCloud(3);
			PointCloud CCoordinates = new PointCloud(3);
			for (int j = 0; j < ND; j++) {
				DCoordinates.addPoint(vectors.get(combinedParameters * i + j));
			}
			for (int j = 0; j < NA; j++) {
				ACoordinates.addPoint(vectors.get(combinedParameters * i + ND + j));
			}
			for (int j = 0; j < NC; j++) {
				CCoordinates.addPoint(vectors.get(combinedParameters * i + ND + NA + j));
			}
			DFrames.add(DCoordinates);
			AFrames.add(ACoordinates);
			CFrames.add(CCoordinates);

		}
    }

}
