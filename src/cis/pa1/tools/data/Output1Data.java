package cis.pa1.tools.data;

import java.util.ArrayList;
import java.util.List;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;

/**
 * An object that stores information from a output1 data file
 * @author John Lee, Kyle Xiong
 *
 */
public class Output1Data extends AbstractDataFileStorage{
	private ColumnVector EMPostVector;
	private ColumnVector opticalPostVector;
	List<PointCloud> CFrames;
	
	
	public ColumnVector getEMPostVector() {
		return EMPostVector;
	}


	public void setEMPostVector(ColumnVector eMPostVector) {
		EMPostVector = eMPostVector;
	}


	public ColumnVector getOpticalPostVector() {
		return opticalPostVector;
	}


	public void setOpticalPostVector(ColumnVector opticalPostVector) {
		this.opticalPostVector = opticalPostVector;
	}


	public List<PointCloud> getCFrames() {
		return CFrames;
	}


	public void setCFrames(List<PointCloud> cFrames) {
		CFrames = cFrames;
	}
	
	public Output1Data() {
		this.CFrames = new ArrayList<>();
	}


	@Override
    public void columnVectorsToPointCloud(List<ColumnVector> vectors, List<Integer> parameters) {
		checkParameterSize(2, parameters, "OUTPUT");
		this.parameters = parameters;	
		int NC = parameters.get(0);
		int NFrames = parameters.get(1);
		this.EMPostVector = vectors.get(0);
		this.opticalPostVector = vectors.get(1);
		for (int i = 0; i < NFrames; i++) {
			PointCloud CCoordinates = new PointCloud(3);
			for (int j = 0; j < NC; j++) {
				CCoordinates.addPoint(vectors.get(NC * i + j + 2));
			}
			CFrames.add(CCoordinates);
		}
    }
	
}
