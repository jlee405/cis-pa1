package cis.pa1.tools.tests;

import org.junit.Assert;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;

public class TestHelper {
	
	/**
	 * Asserts whether two matrices have the same entries
	 * @param expected The expected matrix 
	 * @param actual The actual matrix 
	 * @param errorThreshold The threshold of error between the two
	 */
	 public static void assertMatricesMatch(
			 Matrix expected, Matrix actual, double errorThreshold) {
		Assert.assertEquals(expected.getRowDimension(), actual.getRowDimension());
		Assert.assertEquals(expected.getColumnDimension(), actual.getColumnDimension());
		for (int i = 0; i < expected.getRowDimension(); i++) {
			for (int j = 0; j < expected.getColumnDimension(); j++) {
				Assert.assertEquals(expected.get(i, j), actual.get(i, j), errorThreshold);
			}
		}
	}
	
		/**
		 * Asserts whether two column vectors have the same entries
		 * @param expected The expected vector 
		 * @param actual The actual vector 
		 * @param errorThreshold The threshold of error between the two
		 */
	public static void assertColumnVectorsMatch(
			ColumnVector expected, ColumnVector actual, double errorThreshold) {
		Assert.assertEquals(expected.getDimension(), actual.getDimension());
		for (int i = 0; i < expected.getDimension(); i++) {
			Assert.assertEquals(expected.get(i), actual.get(i), errorThreshold);
		}
	}

}
