package cis.pa1.tools.tests;

import java.io.File;
import java.util.List;

import org.junit.Test;

import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.DistortionCalibration;
import cis.pa1.tools.calibration.drivers.DistortionCalibrationDriver;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.Output1Data;

public class DistortionCalibrationTest {

	@Test
	public void testDistortionCalibration() {
		DistortionCalibrationDriver driver = new DistortionCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		char letter = 'a';
		String bodyPath = "data" + File.separator + "pa1-debug-" + letter + "-calbody.txt";
		String readingPath = "data" + File.separator + "pa1-debug-" + letter + "-calreadings.txt";
		driver.performDistortionCalibration(false, bodyPath, readingPath);
		List<DistortionCalibration> calculatedCFrames = driver.getDistortionCalibrations();
		Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa1-debug-" + letter + "-output1.txt");
		List<PointCloud> expectedCFrames = output.getCFrames();
		
		for (int i = 0; i < calculatedCFrames.size(); i++) {
			TestHelper.assertMatricesMatch(expectedCFrames.get(i).asMatrix(), 
					calculatedCFrames.get(i).expectedC().asMatrix(), 10e-3);
		}
	}
}
