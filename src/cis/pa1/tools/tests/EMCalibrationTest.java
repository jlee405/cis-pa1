package cis.pa1.tools.tests;

import java.io.File;

import org.junit.Test;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.drivers.EMCalibrationDriver;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.Output1Data;

public class EMCalibrationTest {

	@Test
	public void testEMCalibration() {
		EMCalibrationDriver driver = new EMCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		char letter = 'a';
		String path = "data" + File.separator + "pa1-debug-" + letter + "-empivot.txt";
		ColumnVector calculatedPostVector = driver.performEMCalibration(false, path);
		PointCloud postVectors = new PointCloud(3);
		postVectors.addPoint(calculatedPostVector);
		Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa1-debug-" + letter + "-output1.txt");
		postVectors.addPoint(output.getEMPostVector());
		TestHelper.assertColumnVectorsMatch(postVectors.get(1), postVectors.get(0), 10e-2);
	}
}
