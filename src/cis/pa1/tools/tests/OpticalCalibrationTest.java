package cis.pa1.tools.tests;

import java.io.File;

import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.PointCloud;
import cis.pa1.tools.calibration.drivers.OpticalCalibrationDriver;
import cis.pa1.tools.data.DataFileParser;
import cis.pa1.tools.data.Output1Data;

public class OpticalCalibrationTest {
	public void testOpticalCalibration() {
		OpticalCalibrationDriver driver = new OpticalCalibrationDriver();
		DataFileParser parser = new DataFileParser();
		char letter = 'a';
		String optPath = "data" + File.separator + "pa1-debug-" + letter + "-optpivot.txt";
		String bodyPath = "data" + File.separator + "pa1-debug-" + letter + "-calbody.txt";
		ColumnVector calculatedPostVector = driver.performOpticalCalibration(false, optPath, bodyPath);
		PointCloud postVectors = new PointCloud(3);
		postVectors.addPoint(calculatedPostVector);
		Output1Data output = parser.parseOutput1Data("data" + File.separator + "pa1-debug-" + letter + "-output1.txt");
		postVectors.addPoint(output.getOpticalPostVector());
		
		System.out.println("Optical post vector for debug dataset " + letter + " vs. expected");
		TestHelper.assertColumnVectorsMatch(postVectors.get(1), postVectors.get(0), 10e-2);
	}
}
