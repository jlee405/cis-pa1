package cis.pa1.tools.tests;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import Jama.Matrix;
import cis.pa1.tools.ColumnVector;
import cis.pa1.tools.Frame;
import cis.pa1.tools.HornRegistration;
import cis.pa1.tools.PointCloud;

public class PointCloudTests {
	private final double ERROR_THRESHOLD = 10e-5;
	private final Random rand = new Random();
	private final int dimensions = 3;
	private final int numberOfPoints = 6;
	private double theta;
	private Matrix xRotationMatrix, yRotationMatrix, zRotationMatrix;
	private PointCloud cloud1, cloud2;
	private ColumnVector translationVector;
	
	@Before
	public void initialize() {		
		/* Randomly generates points for the point cloud */
		theta = Math.PI * (rand.nextDouble() * 2 - 1);
		this.cloud1 = new PointCloud(dimensions);
		for (int i = 0; i < numberOfPoints; i++) {
			this.cloud1.addPoint(generateRandomDouble(100), 
					generateRandomDouble(100), 
					generateRandomDouble(100));
		}
		
		this.cloud2 = new PointCloud(dimensions);
		for (int i = 0; i < numberOfPoints; i++) {
			this.cloud1.addPoint(generateRandomDouble(100), 
					generateRandomDouble(100), 
					generateRandomDouble(100));
		}
		
		/* Rotation matrices for rotations about x, y, and z, respectively */
		double[][] xRotationArray = {{1,0,0}, 
				{0,Math.cos(theta),-Math.sin(theta)},
				{0,Math.sin(theta),Math.cos(theta)}};
		double[][] yRotationArray = {{Math.cos(theta),0,Math.sin(theta)},
				{0,1,0},
				{-Math.sin(theta),0,Math.cos(theta)}};
		double[][] zRotationArray = {{Math.cos(theta),-Math.sin(theta),0},
				{Math.sin(theta),Math.cos(theta),0},
				{0,0,1}};

		this.xRotationMatrix = new Matrix(xRotationArray);
		this.yRotationMatrix = new Matrix(yRotationArray);
		this.zRotationMatrix = new Matrix(zRotationArray);
		
		/* Generates a random translation vector */
		double[] vector = {generateRandomDouble(100),
				generateRandomDouble(100),
				generateRandomDouble(100)};
		this.translationVector = new ColumnVector(vector);
	}
	
	@Test
	public void testTransform() {
		Matrix transformMatrix = xRotationMatrix.plus(yRotationMatrix).plus(zRotationMatrix);
		cloud2 = cloud1.clone();
		cloud2.transform(transformMatrix);
		for (int i = 0; i < numberOfPoints; i++) {
			TestHelper.assertColumnVectorsMatch(new ColumnVector(
					transformMatrix.times(cloud1.get(i).getMatrix())), cloud2.get(i), ERROR_THRESHOLD);
		}
	}
	
	@Test
	public void test3dRotate() {
		ColumnVector xUnitVector = new ColumnVector(3);
		xUnitVector.setVector(1, 0, 0);
		ColumnVector yUnitVector = new ColumnVector(3);
		yUnitVector.setVector(0, 1, 0);
		ColumnVector zUnitVector = new ColumnVector(3);
		zUnitVector.setVector(0, 0, 1);
		
		/* Rotation of each point cloud using the rotate3d method of 
		 * PointCloud about x, y, and z, respectively */
		PointCloud xRotated = cloud1.clone();
		xRotated.rotate3d(theta, xUnitVector);
		PointCloud yRotated = cloud1.clone();
		yRotated.rotate3d(theta, yUnitVector);
		PointCloud zRotated = cloud1.clone();
		zRotated.rotate3d(theta, zUnitVector);
				
		
		/* Checks to see that each x, y, and z value of the expected and actual values match */
		for (int i = 0; i < numberOfPoints; i++) {
			Matrix currentVector = cloud1.get(i).getMatrix();
			ColumnVector expectedXVector = new ColumnVector(xRotationMatrix.times(currentVector));	
			ColumnVector expectedYVector = new ColumnVector(yRotationMatrix.times(currentVector));	
			ColumnVector expectedZVector = new ColumnVector(zRotationMatrix.times(currentVector));	
			
			ColumnVector actualXVector = xRotated.get(i);
			ColumnVector actualYVector = yRotated.get(i);
			ColumnVector actualZVector = zRotated.get(i);
			TestHelper.assertColumnVectorsMatch(expectedXVector, actualXVector, ERROR_THRESHOLD);
			TestHelper.assertColumnVectorsMatch(expectedYVector, actualYVector, ERROR_THRESHOLD);
			TestHelper.assertColumnVectorsMatch(expectedZVector, actualZVector, ERROR_THRESHOLD);
		}
	}
	
	@Test
	public void testTranslate() {
		cloud2 = cloud1.clone();
		cloud2.translate(translationVector);
		for (int i = 0; i < numberOfPoints; i++) {
			TestHelper.assertColumnVectorsMatch(cloud1.get(i).plus(translationVector), cloud2.get(i), ERROR_THRESHOLD);
		}
	}
	
	@Test
	public void testRegister() {
		cloud2 = cloud1.clone();
		Frame frame = new Frame(xRotationMatrix, translationVector);
		cloud2.frameShift(frame);
		HornRegistration registration = new HornRegistration(cloud1.asMatrix(), cloud2.asMatrix(), false);
		
		TestHelper.assertMatricesMatch(xRotationMatrix, registration.rotationMatrix(), ERROR_THRESHOLD);
		TestHelper.assertColumnVectorsMatch(translationVector, registration.transVector(), ERROR_THRESHOLD);		
	}
	
	private double generateRandomDouble(double absoluteBound) {
		return 2*absoluteBound*(rand.nextDouble() - 0.5);
	}
}