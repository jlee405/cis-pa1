EM navigation points in CT coordinates for dataset d

 0.81 148.04 39.45 129.03
 26.17 55.35 0.97 25.94
 64.39 -3.05 96.21 -4.24

Difference from expected output.

 -103.34 -5.24 -26.18 63.71
 -146.33 0.14 -59.30 -77.87
 -76.80 -100.29 -25.53 -163.53

